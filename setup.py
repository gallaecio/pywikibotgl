from os import makedirs, path, walk
import re
from setuptools import find_packages, setup
from subprocess import run


folder_path = path.dirname(__file__)
git_url = 'https://gitlab.com/gallaecio/pywikibotgl'
version = '0.1'


def create_data_files():
    po_folder = path.join(folder_path, 'po')
    locale_folder = path.join(folder_path, 'pywikibotgl', 'locale')
    data_files = []
    for root, _, file_names in walk(po_folder):
        for file_name in file_names:
            if not file_name.endswith('.po'):
                continue
            install_dir = path.join(
                'share', 'locale', file_name[:-3], 'LC_MESSAGES')
            mo_folder = path.join(locale_folder, file_name[:-3], 'LC_MESSAGES')
            makedirs(mo_folder, exist_ok=True)
            mo_file = path.join(mo_folder, 'pywikibotgl.mo')
            run(['msgfmt', path.join(root, file_name), '-o', mo_file])
            data_files.append((install_dir, (mo_file,)))
        break
    return data_files


def long_description():
    with open(path.join(folder_path, 'README.rst')) as f:
        return f.read()


setup(
    name='pywikibotgl',
    version=version,
    description='Tool based on PyWikiBot to perform automated and ' \
                'semiautomated tasks on Galician MediaWiki sites of the ' \
                'Wikimedia Foundation.',
    long_description=long_description(),
    url='https://gl.wikipedia.org/wiki/Wikipedia:Pywikibotgl',
    download_url='{}/repository/archive.tar.gz?ref=v{}'.format(
        git_url, version),
    author='Adrián Chaves (Gallaecio)',
    author_email='adrian@chaves.io',
    license='AGPLv3+',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: Console',
        'Intended Audience :: Other Audience',
        'License :: OSI Approved :: GNU Affero General Public License v3 or '
            'later (AGPLv3+)',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Topic :: Other/Nonlisted Topic',
    ],
    packages=find_packages('.'),
    install_requires=[
        'click',
        'lxml',
        'pyxdg',
        'pywikibot',
        'requests',
        'sphinx',
        'sphinx-intl',
    ],
    entry_points={
        'console_scripts': [
            'pywikibotgl-population = pywikibotgl.population:main',
        ],
    },
    data_files=create_data_files()
)
