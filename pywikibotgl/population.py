#!/usr/bin/env python3

"""Script to update the population data in articles about Galician locations
according to the data of the Galician Institute of Statistics (IGE)."""

from os import path, makedirs
import re
from sys import exit
from warnings import catch_warnings, simplefilter

from click import argument, command, echo, style, ClickException
from lxml import html
from requests import get
from xdg.BaseDirectory import save_cache_path

with catch_warnings():
    simplefilter('ignore')
    from pywikibot import input_choice, showDiff, Category, Page

from pywikibot.bot import open_webbrowser
from pywikibot.bot_choice import QuitKeyboardInterrupt
from pywikibot.i18n import TranslationError
from pywikibot.pagegenerators import (CategorizedPageGenerator,
                                      SubCategoriesPageGenerator)

from .locale import _
from .site import Site


class _MultipleMatches(RuntimeError):

    def __init__(self, *matches):
        self.matches = matches
        super().__init__()


class _NotALocation(RuntimeError):
    pass


class _NotAParish(RuntimeError):
    pass


class _NotCached(RuntimeError):
    pass


class _NotFound(RuntimeError):
    pass


def _year():
    response = get('http://www.ige.eu/igebdt/esq.jsp?page=002001'
                   '&ruta=nomenclator/nomenclator.jsp')
    tree = html.fromstring(response.content)
    year = int(tree.xpath('//select[@name="ano"]/option[@selected]/text()')[0])
    echo(style(_('Latest available population data is from {year}.').format(
        year=style(str(year), fg='green')), bold=True))
    return year


def _cached_data(directory, file_name):
    if not path.exists(directory):
        makedirs(directory)
        raise _NotCached
    if not path.isdir(directory):
        raise ClickException(_('The cache directory ({}) is a file, not a '
                               'directory.').format(directory))
    file_path = path.join(directory, file_name)
    if not path.exists(file_path):
        raise _NotCached
    echo(style(_('Using {cached} data.').format(
        cached=style(_('cached'), fg='green')), bold=True))
    with open(file_path, 'rb') as data_file:
        return data_file.read()


def _download_data(directory, file_name, year):
    echo(style(_('Downloading the latest data…'), bold=True))
    response = get('http://www.ige.eu/igebdt/servlet/nomenclatorexcel?ANO={}'
                   '&P=0&C=0&EC=1&ES=1&NU=1&TNU=0&CT=%3E&T=&NOM='.format(year))
    data = response.content
    with open(path.join(directory, file_name), 'wb') as data_file:
        data_file.write(data)
    echo(style(_('Using {downloaded} data.').format(
        downloaded=style(_('downloaded'), fg='green', bold=True)), bold=True))
    return data


def _lines(year):
    directory = save_cache_path('wikipedia-gl-galician-population-updater')
    file_name = '{}.txt'.format(year)
    try:
        data = _cached_data(directory, file_name)
    except _NotCached:
        data = _download_data(directory, file_name, year)
    lines = iter(data.decode('iso-8859-15').splitlines())
    for _ in range(4):
        try:
            next(lines)
        except StopIteration:
            return
    for line in lines:
        if line:
            yield line
        else:
            return


def _line_parish(data, province, town, cells):
    parish = cells[7].strip()
    if parish not in data[province][town]:
        data[province][town][parish] = {'locations': {},
                                        'population': int(cells[8])}
    return parish


def _data(year):
    data = {}
    current_province = None
    current_town = None
    current_parish = None
    current_location = None
    upgrade_sublocations = False
    for line in _lines(year):
        cells = line.split('\t')
        province = cells[5]
        if current_province != province:
            assert province not in data
            data[province] = {}
            current_province = province
            current_town = None
            current_parish = None
            current_location = None
        town = cells[6].strip()
        if current_town == 'CERDEDO-COTOBADE' and town == 'null':
            town = 'CERDEDO-COTOBADE'
        if current_town != town:
            if town == 'null':
                # Source data issue detected with the last town of Pontevedra,
                # ‘CERDEDO-COTOBADE’.
                assert current_town == 'ILLA DE AROUSA, A'
                town = 'CERDEDO-COTOBADE'
            assert town not in data[province]
            data[province][town] = {}
            current_town = town
            current_parish = None
            current_location = None
        if cells[2] == '00':
            current_parish = _line_parish(data, province, town, cells)
            current_location = None
        if cells[3] == '00':
            current_parish = _line_parish(data, province, town, cells)
            current_location = None
        elif cells[4] == '00':
            upgrade_sublocations = False
            location = cells[7].strip()
            if current_location == location:
                # e.g. ESCAÑOI, OS ODREIROS, AMBROA (SAN TIRSO), IRIXOA,
                #      A CORUÑA
                upgrade_sublocations = True
                continue
            population = int(cells[8])
            if location not in data[province][town][current_parish]['locations'] or \
                    data[province][town][current_parish]['locations'][location] > population:
                data[province][town][current_parish]['locations'][location] = population
            current_location = location
        elif upgrade_sublocations:
            location = cells[7].strip()
            population = int(cells[8])
            if location not in data[province][town][current_parish]['locations'] or \
                    data[province][town][current_parish]['locations'][location] > population:
                data[province][town][current_parish]['locations'][location] = population
            current_location = location
    return data


def _root_categories(site, year):
    title_re = re.compile(r'^Poboación (\d+)$')
    for category in SubCategoriesPageGenerator(
            Category(site, 'Categoría:Wikipedia:Poboación')):
        title = category.title(withNamespace=False)
        match = title_re.match(title)
        if not match:
            continue
        category_year = int(match.group(1))
        if category_year >= year:
            continue
        yield category
    yield Category(site, 'Categoría:Galicia sen datos de poboación')


PROVINCE_RE = (
    (re.compile(r'\[\[\s*[Pp]rovincia\s+da\s+Coruña\s*(\|[^]]*)?\]\]'),
     'A Coruña'),
    (re.compile(r'\[\[\s*[Pp]rovincia\s+de\s+Lugo\s*(\|[^]]*)?\]\]'),
     'Lugo'),
    (re.compile(r'\[\[\s*[Pp]rovincia\s+de\s+Ourense\s*(\|[^]]*)?\]\]'),
     'Ourense'),
    (re.compile(r'\[\[\s*[Pp]rovincia\s+de\s+Pontevedra\s*(\|[^]]*)?\]\]'),
     'Pontevedra'),
)


def _province(page):
    provinces = []
    content = page.get()
    for province_re, province in PROVINCE_RE:
        if province_re.search(content):
            provinces.append(province)
    if len(provinces) != 1:
        return None
    return provinces[0]


_PARISH_RE = re.compile(
    r'(?s)(?:^|\n)\{\{\s*Parroquia\b(.*?)\n\}\}')
_PARISH_NAME_RE = re.compile(
    r'\n\s*\|\s*nome\s*=\s*([^\s].*[^\s])\s*\n')
_PARISH_TOWN_RE = re.compile(
    r'\n\s*\|\s*town\s*=\s*([^\s].*[^\s])\s*\n')
_LOCATION_RE = re.compile(
    r'(?s)(?:^|\n)\{\{\s*Lugar\b(.*?)\n\}\}')
_LOCATION_NAME_RE = re.compile(
    r'\n\s*\|\s*nome\s*=\s*([^\s].*[^\s])\s*\n')
_LOCATION_PARISH_RE = re.compile(
    r'\n\s*\|\s*parroquia\s*=\s*([^\s].*[^\s])\s*\n')
_LOCATION_TOWN_RE = re.compile(
    r'\n\s*\|\s*town\s*=\s*([^\s].*[^\s])\s*\n')


def _location(page):
    content = page.get()
    match = _LOCATION_RE.search(content)
    if not match:
        raise _NotALocation
    match = _LOCATION_NAME_RE.search(content)
    if not match:
        raise ClickException(
            _('No location name at {}').format(page.title()))
    location = match.group(1)
    match = _LOCATION_PARISH_RE.search(content)
    if not match:
        raise ClickException(
            _('No location parish at {}').format(page.title()))
    parish = match.group(1)
    match = _LOCATION_TOWN_RE.search(content)
    if not match:
        raise ClickException(
            _('No location town at {}').format(page.title()))
    town = match.group(1)
    return town, parish, location


def _parish(page):
    content = page.get()
    match = _PARISH_RE.search(content)
    if not match:
        raise _NotAParish
    match = _PARISH_NAME_RE.search(content)
    if not match:
        raise ClickException(_('No parish name at {}').format(page.title()))
    parish = match.group(1)
    match = _PARISH_TOWN_RE.search(content)
    if not match:
        raise ClickException(_('No parish town at {}').format(page.title()))
    town = match.group(1)
    return town, parish, None


def _town_parish_location(page):
    try:
        return _location(page)
    except _NotALocation:
        pass
    try:
        return _parish(page)
    except _NotAParish:
        pass
    raise ClickException(_('This script cannot determine whether {} is a '
                           'location or a parish.').format(page.title()))


SAINT_NAME_RE = re.compile(
    r'(?i)^\s*(San(?:(?:t[ao])?\s+.+?|tiago))\s+d(?:e|[ao]s?)\s+(.+?)\s*$')


def _escape(string):
    string = string.lower()
    string = re.sub(r'^\s*[ao]s?\s+', '', string)
    escaped = re.escape(string)
    # Keep distinguishing between accented and regular vocals (e.g. a != á),
    # because there are cases where there is a difference, such as:
    # VALLO, SAN VICENTE DE FERVENZAS (SAN VICENTE), ARANGA, A CORUÑA
    # O VALLÓ, SAN VICENTE DE FERVENZAS (SAN VICENTE), ARANGA, A CORUÑA
    escaped = escaped.replace(r'\\ ', r'\s+')
    return escaped


def _name_re(name):
    match = SAINT_NAME_RE.search(name)
    if match:
        # Covered cases:
        #
        # -   If name is ‘SAN XIAO DE MONTOXO’, it must match ‘MONTOXO (SAN
        #     XIAO)’.
        #
        # -   If name is ‘SANTA MARIÑA DE ALBÁN’, it must match ‘SANTA MARIÑA
        #     DE ALBÁN (SANTA MARIÑA)’ but not ‘ALBÁN (SAN PAIO)’.
        pattern = r'(?i)^\s*((([ao]s?\s+)?{name}|{name}\s*,\s*[ao]s?)\s*(\([' \
                  r'^)]*\)\s*)?|(([ao]s?\s+)?{of_name}|{of_name}\s*,\s*[ao]s' \
                  r'?)\s+\({saint_name}\)|{name}\s+\({saint_name}\))$'
        return re.compile(pattern.format(name=_escape(name),
                                         saint_name=_escape(match.group(1)),
                                         of_name=_escape(match.group(2))))
    pattern = r'(?i)^\s*(([ao]s?\s+)?{name}|{name}\s*,\s*[ao]s?)\s*(\([^)]*' \
              r'\)\s*)?$'
    return re.compile(pattern.format(name=_escape(name)))


def _population_and_province(data, provinces, town_re, parish_re, location_re):
    population = None
    province_found = None
    match = None
    for _province in provinces:
        for _town in data[_province].keys():
            if not town_re.match(_town):
                continue
            for _parish in data[_province][_town].keys():
                if not parish_re.match(_parish):
                    continue
                if location_re is None:
                    full_name = ', '.join((_parish, _town, _province))
                    if match is not None:
                        raise _MultipleMatches(match, full_name)
                    match = full_name
                    population = data[_province][_town][_parish]['population']
                    province_found = _province
                    continue
                for _location in data[_province][_town][_parish]['locations'].keys():
                    if not location_re.match(_location):
                        continue
                    full_name = ', '.join((_location, _parish, _town, _province))
                    if match is not None:
                        raise _MultipleMatches(match, full_name)
                    match = full_name
                    population = data[_province][_town][_parish]['locations'][_location]
                    province_found = _province
    return population, province_found


def _page_data(page, data):
    province = _province(page)
    town, parish, location = _town_parish_location(page)
    if location is not None:
        location_re = _name_re(location)
    else:
        location_re = None
    if province:
        provinces = (province.upper(),)
    else:
        provinces = tuple(data.keys())
    try:
        population, province_found = _population_and_province(
            data, provinces, _name_re(town), _name_re(parish), location_re)
    except _MultipleMatches as error:
        raise ClickException(
            _('Multiple matches found for page \'{title}\':\n{matches}').format(
                title=page.title(),
                matches='\n'.join(
                    '({index}) {match}'.format(index=i, match=match)
                    for i, match in enumerate(error.matches, start=1))))
    if population is None:
        raise _NotFound
    if province is None:
        for _re, known_province in PROVINCE_RE:
            if known_province.upper() == province_found:
                province = known_province
                break
    return province, town, population


_CATEGORY_RE = re.compile(
    r'\[\[\s*Categor(ía|y)\s*:\s*[^]]+\s+poboación\s+\d+\]\]'
    r'\s*(\n|$)')
_POPULATION_RE = re.compile(
    r'(\n\s*\|\s*poboación\s*=)[^{]*?(\n|\})')
_YEAR_RE = re.compile(
    r'(\n\s*\|\s*ano\s*=)\s*\d*\s*\n')
_SUMMARY = 'Actualizo o dato de poboación'

CATEGORY = '{{{{Categoría oculta}}}}\n\n[[Categoría:{} poboación {}|{}]]\n'
SORT_BY_RE = re.compile(r'(?i)^\s*(?:[ao]s?\s+)?(.*?)\s*$')


def _sort_by(text):
    return SORT_BY_RE.match(text).group(1)


def _send_changes(page, contido, description, accept_all):
    if page.exists():
        contido_vello = page.get()
    else:
        contido_vello = ''
    if contido_vello == contido:
        return accept_all
    echo('\n' + style(page.title(), bold=True, fg='magenta'))
    showDiff(contido_vello, contido)
    if not accept_all:
        while True:
            try:
                escolla = input_choice(
                    _('Do you want to submit these changes?'),
                    [(_('Yes'), 'y'), (_('Always'), 'a'), (_('No'), 'n'),
                     (_('Open in Web Browser'), 'b')], 'n')
            except QuitKeyboardInterrupt:
                exit(_('Script execution aborted by user'))
            if escolla == 'n':
                return accept_all
            elif escolla == 'b':
                try:
                    open_webbrowser(page)
                except TranslationError:
                    pass
            elif escolla == 'y':
                break
            elif escolla == 'a':
                accept_all = True
                break
            else:
                assert False
    page.text = contido
    page.save(summary=description, minor=True, quiet=True)
    return accept_all


def _iter_pages(pages, wikipedia_gl, year):
    if pages:
        for page in pages:
            yield Page(wikipedia_gl, page)
    else:
        for category in _root_categories(wikipedia_gl, year):
            yield from CategorizedPageGenerator(
                category, recurse=True, content=True)



@command()
@argument('pages', metavar=_('pages'), nargs=-1)
def main(pages):
    """Updates the population data of the specified list of names of pages
    of Galician loactions in the Galician Wikipedia.

    If no page name is specified, all location pages are updated.

    The update process is interactive, the user is asked for confirmation
    before every change.
    """
    year = _year()
    data = _data(year)
    wikipedia_gl = Site()
    wikipedia_gl.login()
    accept_all = False
    towns_with_category = set()
    for page in _iter_pages(pages, wikipedia_gl, year):
        try:
            province, town, location = _page_data(page, data)
        except _NotFound:
            echo(_('Warning: Could not find a population match for page '
                   '\'{}\'.').format(page.title()))
            return
        population = '{:,}'.format(location).replace(',', '.')
        content = page.get()
        if not _POPULATION_RE.search(content):
            raise RuntimeError(page.title())
        content = _POPULATION_RE.sub('\\g<1> {}\\g<2>'.format(population),
                                     content)
        if not _YEAR_RE.search(content):
            raise RuntimeError(page.title())
        content = _YEAR_RE.sub('\\g<1> {}\n'.format(year), content)
        content = _CATEGORY_RE.sub('', content)
        accept_all = _send_changes(page, content, _SUMMARY, accept_all)
        if town not in towns_with_category:
            category = Category(
                wikipedia_gl,
                'Categoría:{} poboación {}'.format(town, year))
            if not category.exists() and not category.isEmptyCategory():
                _content = CATEGORY.format(province, year, _sort_by(town))
                description = 'Creo para aloxar páxinas como «{}».'.format(
                    page.title())
                accept_all = _send_changes(
                    category, _content, description, accept_all)
            towns_with_category.add(town)
