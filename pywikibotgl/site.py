from pywikibot import error, input, log, output, Site as _Site
from pywikibot.data.api import APIError, LoginManager as _LoginManager
from pywikibot.exceptions import NoUsername
from pywikibot.site import APISite as _APISite, LoginStatus

from .locale import _


class LoginManager(_LoginManager):
    """Custom LoginManager subclass to provide localized messages during
    login."""

    def login(self, retry=False, autocreate=False):
        """
        Attempt to log into the server.

        @param retry: infinitely retry if the API returns an unknown error
        @type retry: bool

        @param autocreate: if true, allow auto-creation of the account
                           using unified login
        @type autocreate: bool

        @raises NoUsername: Username is not recognised by the site.
        """
        if not self.password:
            # First check that the username exists,
            # to avoid asking for a password that will not work.
            if not autocreate:
                self.check_user_exists()

            # As we don't want the password to appear on the screen, we set
            # password = True
            self.password = input(
                _('Password for user {name} on {site} (no characters will be '
                  'shown):').format(name=self.login_name, site=self.site),
                password=True)

        output(_('Logging in to {site} as {name}').format(
            name=self.login_name, site=self.site))
        try:
            cookiedata = self.getCookie()
        except APIError as e:
            error(u"Login failed (%s)." % e.code)
            if e.code == 'NotExists':
                raise NoUsername(u"Username '%s' does not exist on %s"
                                 % (self.login_name, self.site))
            elif e.code == 'Illegal':
                raise NoUsername(u"Username '%s' is invalid on %s"
                                 % (self.login_name, self.site))
            elif e.code == 'readapidenied':
                raise NoUsername(
                    'Username "{0}" does not have read permissions on '
                    '{1}'.format(self.login_name, self.site))
            elif e.code == 'Failed':
                raise NoUsername(
                    'Username "{0}" does not have read permissions on '
                    '{1}\n.{2}'.format(self.login_name, self.site, e.info))
            # TODO: investigate other unhandled API codes (bug T75539)
            if retry:
                self.password = None
                return self.login(retry=True)
            else:
                return False
        self.storecookiedata(cookiedata)
        log(u"Should be logged in now")
#        # Show a warning according to the local bot policy
#   FIXME: disabled due to recursion; need to move this to the Site object after
#   login
#        if not self.botAllowed():
#            logger.error(
#                u"Username '%(name)s' is not listed on [[%(page)s]]."
#                 % {'name': self.username,
#                    'page': botList[self.site.family.name][self.site.code]})
#            logger.error(
# "Please make sure you are allowed to use the robot before actually using it!")
#            return False
        return True


class APISite(_APISite):
    """Custom APISite subclass to provide localized messages during login."""

    def login(self, sysop=False, autocreate=False):
        """
        Log the user in if not already logged in.

        @param sysop: if true, log in with the sysop account.
        @type sysop: bool

        @param autocreate: if true, allow auto-creation of the account
                           using unified login
        @type autocreate: bool

        @raises NoUsername: Username is not recognised by the site.
        @see: U{https://www.mediawiki.org/wiki/API:Login}
        """
        # TODO: this should include an assert that loginstatus
        #       is not already IN_PROGRESS, however the
        #       login status may be left 'IN_PROGRESS' because
        #       of exceptions or if the first method of login
        #       (below) is successful. Instead, log the problem,
        #       to be increased to 'warning' level once majority
        #       of issues are resolved.
        if self._loginstatus == LoginStatus.IN_PROGRESS:
            pywikibot.log(
                u'%r.login(%r) called when a previous login was in progress.'
                % (self, sysop)
            )
        # There are several ways that the site may already be
        # logged in, and we do not need to hit the server again.
        # logged_in() is False if _userinfo exists, which means this
        # will have no effect for the invocation from api.py
        if self.logged_in(sysop):
            self._loginstatus = (LoginStatus.AS_SYSOP
                                 if sysop else LoginStatus.AS_USER)
            return
        # check whether a login cookie already exists for this user
        # or check user identity when OAuth enabled
        self._loginstatus = LoginStatus.IN_PROGRESS
        try:
            self.getuserinfo(force=True)
            if self.userinfo['name'] == self._username[sysop] and \
               self.logged_in(sysop):
                return
        # May occur if you are not logged in (no API read permissions).
        except APIError:
            pass
        except NoUsername as e:
            if not autocreate:
                raise e

        if self.is_oauth_token_available():
            if sysop:
                raise NoUsername('No sysop is permitted with OAuth')
            elif self.userinfo['name'] != self._username[sysop]:
                if self._username == [None, None]:
                    raise NoUsername('No username has been defined in your '
                                     'user-config.py: you have to add in this '
                                     'file the following line:\n'
                                     "usernames['{family}']['{lang}'] "
                                     "= '{username}'"
                                     .format(family=self.family,
                                             lang=self.lang,
                                             username=self.userinfo['name']))
                else:
                    raise NoUsername('Logged in on {site} via OAuth as '
                                     '{wrong}, but expect as {right}'
                                     .format(site=self,
                                             wrong=self.userinfo['name'],
                                             right=self._username[sysop]))
            else:
                raise NoUsername('Logging in on %s via OAuth failed' % self)
        login_manager = LoginManager(
            site=self, sysop=sysop, user=self._username[sysop])
        if login_manager.login(retry=True, autocreate=autocreate):
            self._username[sysop] = login_manager.username
            self.getuserinfo(force=True)
            self._loginstatus = (LoginStatus.AS_SYSOP
                                 if sysop else LoginStatus.AS_USER)
        else:
            self._loginstatus = LoginStatus.NOT_LOGGED_IN  # failure


def Site(family='wikipedia', code='gl', **kwargs):
    return _Site(code=code, fam=family, interface=APISite, **kwargs)
