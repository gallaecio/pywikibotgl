from gettext import translation
from locale import getdefaultlocale
from os import path
from sys import prefix


_LOCALE, _ = getdefaultlocale()
_LOCALEDIR = path.join(path.abspath(path.dirname(__file__)))
if not path.isdir(_LOCALEDIR):
    _LOCALEDIR = path.join(prefix, 'share', 'locale')

_ = translation('pywikibotgl', _LOCALEDIR, [_LOCALE, _LOCALE[:2]]).gettext
