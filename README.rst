MediaWiki Bot Framework for Galician Projects
=============================================

Framework on top of `Pywikibot
<https://www.mediawiki.org/wiki/Manual:Pywikibot>`_ to operate on MediaWiki
sites in Galician.

The framework includes a script to update the population data on articles about
locations within Galicia.

Usage
-----

To run any script of the framework, you must install the framework as follows:

#.  Download the source code.

#.  Install Python 3.

#.  Install on a Python 3 virtual environment:

    For example, on Linux::

        python3 -m venv venv
        . venv/bin/activate
        pip install -e .

    .. note:: On Windows, you may optionally install :code:`colorama` to enjoy
              colors on the command-line interface::

                  pip install colorama

#.  Create a :code:`user-config.py` file with the following content::

        family = 'wikipedia'
        mylang = 'gl'
        usernames['wikipedia']['*'] = '<Your Galician Wikipedia Username>'
        password_file = 'user-password.py'

#.  Create a :code:`user-password.py` file as described in the
    `PyWikiBot documentation
    <https://www.mediawiki.org/wiki/Manual:Pywikibot/BotPasswords#Configuration>`_.

    Do not prefix strings with :code:`u` (e.g. :code:`u'asdf'`) as described in
    the documentation. That is for Python 2. In Python 3 you should remove it
    (e.g. :code:`'asdf'`).

#.  Run any script. For example::

        pywikibotgl-population

Once these initial steps are done, to run a script again after you close the
current terminal you must always first re-enable the virtual environment::

    . venv/bin/activate

Development
-----------

Automated Tests
'''''''''''''''

To run the automated tests::

        python3 -m venv venv
        . venv/bin/activate
        pip install -e .
        pip install pytest pytest-pylint
        pytest --pylint


Internationalization and Localization
'''''''''''''''''''''''''''''''''''''

To update the translation templates::

    bash utils/update-pot.sh

To update the translation files::

    bash utils/update-po.sh
