extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.coverage',
    'sphinx.ext.viewcode',
]
templates_path = ['_templates']
source_suffix = ['.rst']
master_doc = 'index'
project = 'Pywikibotgl'
copyright = "2018 Adrián Chaves (Gallaecio)"
author = "Adrián Chaves (Gallaecio)"
exclude_patterns = ['_build']
pygments_style = 'sphinx'
todo_include_todos = False
html_theme = 'alabaster'
html_static_path = []
htmlhelp_basename = 'pywikibotgldoc'
latex_elements = {}
latex_documents = [
    (master_doc, 'pywikibotgl.tex', 'Pywikibotgl Documentation',
     "Adrián Chaves (Gallaecio)", 'manual'),
]
man_pages = [
    (master_doc, 'pywikibotgl', 'Pywikibotgl Documentation',
     [author], 1)
]
texinfo_documents = [
    (master_doc, 'pywikibotgl', 'Pywikibotgl Documentation',
     author, 'pywikibotgl', 'One line description of project.',
     'Miscellaneous'),
]
locale_dirs = ['locale/']
gettext_compact = False
