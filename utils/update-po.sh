#!/usr/bin/env bash

original_folder="$(pwd)"

function restore_folder() {
    cd "$original_folder"
    deactivate &> /dev/null
}
trap restore_folder EXIT

root_folder="$(cd "$(dirname "${BASH_SOURCE[0]}")/.." && pwd)"

for po_file in "$root_folder"/po/*.po
do
    msgmerge --update --backup off \
        "$po_file"  "$root_folder/po/pywikibotgl.pot"
done

deactivate &> /dev/null
. venv/bin/activate

cd "$root_folder/docs"
make gettext
