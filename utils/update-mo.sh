#!/usr/bin/env bash

folder="$(cd "$(dirname "${BASH_SOURCE[0]}")/.." && pwd)"

for po_file in "$folder"/po/*.po
do
    locale="${po_file##*/}"
    locale="${locale%.*}"
    msgfmt "$po_file" \
        --output "$folder/pywikibotgl/locale/$locale/LC_MESSAGES/pywikibotgl.mo"
done
