#!/usr/bin/env bash

original_folder="$(pwd)"

function restore_folder() {
    cd "$original_folder"
    deactivate &> /dev/null
}
trap restore_folder EXIT

root_folder="$(cd "$(dirname "${BASH_SOURCE[0]}")/.." && pwd)"
cd "$root_folder"

find pywikibotgl -name "*.py" \
    | xgettext \
        --files-from - \
        --from-code UTF-8 \
        --language Python \
        --copyright-holder "pywikibotgl Contributors" \
        --package-name pywikibotgl \
        --package-version 0.1 \
        --msgid-bugs-address adrian@chaves.io \
        --output po/pywikibotgl.pot

deactivate &> /dev/null
. venv/bin/activate

cd "$root_folder/docs"
sphinx-intl update -p _build/locale -l gl
